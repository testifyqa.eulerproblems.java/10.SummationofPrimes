import org.junit.Assert;

public class SummationOfPrimes {

    public static void main(String[] args) {
        tests(10, 17);
        tests(2000000, 142913828922L);
    }

    private static long sumOfPrimesUpto(long n) {
        long sumOfPrimes = 0;
        long prime = 1;

        while (prime < n) {
            prime++;
            if (isPrime(prime)) {
                sumOfPrimes += prime;
            }
        }
        System.out.println("The sum of primes up to the " + n + "th number is: " + sumOfPrimes);

        return sumOfPrimes;
    }

    private static boolean isPrime(long prime) {
        if (prime < 2) return false;
        else if (prime < 3) return true;

        for (int i = 2; i < Math.sqrt(prime) + 1; i++) {
            if (prime % i ==0) {
                return false;
            }
        }
        return true;
    }

    private static void tests(long n, long sumOfProduct) {
        Assert.assertEquals(sumOfPrimesUpto(n), sumOfProduct);
    }
}
